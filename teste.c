#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>
#define MAXBULLETS 1000

int enemymove = 1;

typedef struct _player
{
	float x,y;
	float w,h;
	int life;
	char name[20];
	int score;
	int speed;
} Player;

typedef struct _parede
{
	int x,y,w,h;
}Parede;

typedef struct _bullet
{
	int on;
	float x;
	float y;
	float xinit;
	float yinit;
	float destinationx;
	float destinationy;
	int speed;
}Bullet;

Bullet bullet[MAXBULLETS];

typedef struct _gamestate
{
	int playerup,playerdown,playerleft,playerright;
	Player player;
	Player enemy;
	Parede parede;
	int ScreenH;
	int ScreenW;
	int mousebuttonl;
	int mousebuttonr;
	float mousex;
	float mousey;
	int rof;
	

}Gamestate;


void init (SDL_Window**,SDL_Renderer**,Gamestate *);

void closing(SDL_Window**,SDL_Renderer**);

void recebeImput(SDL_Event *, Gamestate *);

void desenha(SDL_Renderer*,Gamestate *);

void logica(Gamestate *);

void createbullet(Gamestate* );

void updatebullet(Gamestate *);


int main(int argc, char const *argv[])
{
	
	Gamestate gamestate;
	

	SDL_Window *window = NULL ; 
	SDL_Renderer *renderer = NULL ;
	

	init(&window,&renderer,&gamestate);

	int running = 1;


	while(running == 1)
	{
		//running = checkquit();

		SDL_Event event;

		while (SDL_PollEvent(&event)) {
			switch (event.type) {
				case SDL_QUIT:
					running = 0;
					break;
				case SDL_KEYDOWN:
					if(SDLK_ESCAPE == event.key.keysym.sym)
						running = 0;
					break;
			}

			recebeImput(&event, &gamestate);
		}

		logica(&gamestate);

		desenha(renderer,&gamestate);

		SDL_Delay(1000/60);

		printf("Player life: %d\n",gamestate.player.life );
		printf("Enemy life: %d\n\n\n",gamestate.enemy.life);

		
		if(gamestate.enemy.life <= 0)
		{
			SDL_Texture *win = IMG_LoadTexture(renderer, "win.png");
			SDL_RenderCopy(renderer, win,NULL, 0);
			SDL_RenderPresent(renderer);
			SDL_RenderClear(renderer);   
			printf("YOU WIN!\n\n\n");
			SDL_Delay(3000);
			
			running = 0;
		}
		if(gamestate.player.life <= 0)
		{
			SDL_Texture *lose = IMG_LoadTexture(renderer, "lose.png");
			SDL_RenderCopy(renderer, lose,NULL, 0);
			SDL_RenderPresent(renderer);
			SDL_RenderClear(renderer);  
			printf("YOU LOSE\n"); 
			SDL_Delay(3000);

			running = 0;
		}

	}

	closing(&window,&renderer);
	
    
    return 0;
}



void init(SDL_Window **window,SDL_Renderer **renderer,Gamestate *gamestate)
{

	gamestate->ScreenW = 1024;
	gamestate->ScreenH = 768;	



	 if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        printf("Erro ao iniciar SDL! Erro: %s\n",SDL_GetError() );
        SDL_Quit();
        exit(1);
    }
    else
    {
       * window = SDL_CreateWindow( "Jogo",
                                    0,
                                    0,
                                    
                                   	gamestate->ScreenW,
                                   	gamestate->ScreenH,
                                   	SDL_WINDOW_SHOWN|
									SDL_WINDOW_RESIZABLE|
									SDL_WINDOW_FULLSCREEN_DESKTOP);
        if (*window == NULL)
        {
            printf("Erro ao criar a janela! Erro: %s\n",SDL_GetError());
            SDL_Quit();
            exit(1);
        }
        else
        {
            *renderer = SDL_CreateRenderer(	*window,
            								-1,
            								SDL_RENDERER_ACCELERATED);
        	SDL_RenderSetLogicalSize(*renderer, gamestate->ScreenW, gamestate->ScreenH);
        }
    }

    gamestate->parede.x = 100 ;
    gamestate->parede.y = 100 ;
    gamestate->parede.h = gamestate->ScreenH-200;
    gamestate->parede.w = gamestate->ScreenW-200;

    gamestate->player.w = 75;
    gamestate->player.h = 45;
    gamestate->player.x = (gamestate->ScreenW/2) + (gamestate->player.w/2);
    gamestate->player.y = (gamestate->ScreenH/2) + (gamestate->player.h/2);
    gamestate->player.life = 10;
    gamestate->player.score = 0;
    gamestate->player.speed = 10;
    gamestate->playerup = 0;
    gamestate->playerdown = 0;
    gamestate->playerleft = 0;
    gamestate->playerright = 0;

    gamestate->enemy.w = 75;
    gamestate->enemy.h = 45;
    gamestate->enemy.x = 930;
    gamestate->enemy.y = 384;
    gamestate->enemy.life = 300;
    gamestate->enemy.score = 0;
    gamestate->enemy.speed = 7;

    gamestate->mousex = 600;
    gamestate->mousey = 600;

    int i;
    for(i = 0; i < MAXBULLETS; i++)
    	bullet[i].on = 0;
    SDL_ShowCursor(SDL_DISABLE);

}

void closing(SDL_Window **window,SDL_Renderer **renderer)
{
	SDL_DestroyRenderer(*renderer);
    SDL_DestroyWindow(*window);

    SDL_Quit();
}

void desenha(SDL_Renderer* renderer, Gamestate *gamestate)

{
	
	SDL_Texture *backgroundtex = IMG_LoadTexture(renderer, "background.png");
	SDL_RenderCopy(renderer, backgroundtex,NULL, 0);                         
                                 

    SDL_Rect playerRect = {	gamestate->player.x,gamestate->player.y,
							gamestate->player.w,gamestate->player.h};
	SDL_Texture *playertex = IMG_LoadTexture(renderer, "player.png");
	SDL_RenderCopy(renderer, playertex,NULL, &playerRect);

	if(gamestate->enemy.life > 0)
	{
		SDL_Rect enemyRect = {	gamestate->enemy.x,gamestate->enemy.y,
								gamestate->enemy.w,gamestate->enemy.h};
		SDL_Texture *enemytex = IMG_LoadTexture(renderer, "enemy.png");
		SDL_RenderCopy(renderer, enemytex,NULL, &enemyRect);
	}
	


	SDL_Rect mouseRect = {gamestate->mousex,gamestate->mousey,20,20};
	SDL_Texture *mousetex = IMG_LoadTexture(renderer, "mouse.png");
	SDL_RenderCopy(renderer, mousetex, NULL, &mouseRect);

	int i;
	for(i = 0; i< MAXBULLETS; i++)
	{
		if(bullet[i].on == 1)
		{
			SDL_Rect bulletRect = {bullet[i].x,bullet[i].y,5,5};
			SDL_Texture *bullettex = IMG_LoadTexture(renderer, "bullet.png");
			SDL_RenderCopy(renderer, bullettex, NULL, &bulletRect);
		}
	}
	
	//drawbullet(renderer);

	SDL_Rect playerlifeRect = {100,50,gamestate->player.life * 30, 10};
	SDL_SetRenderDrawColor(renderer, 100, 255, 100, 255);
	SDL_RenderFillRect(renderer, &playerlifeRect);

	SDL_Rect enemylifeRect = {1024/2+100,50,gamestate->enemy.life , 10};
	SDL_SetRenderDrawColor(renderer, 255, 100, 100, 255);
	SDL_RenderFillRect(renderer, &enemylifeRect);

	SDL_RenderPresent(renderer);
	SDL_RenderClear(renderer);    

}

void recebeImput(SDL_Event *event, Gamestate *gamestate)
{
	if(event->type == SDL_KEYDOWN)
	{
		if(SDLK_s == event->key.keysym.sym) 
           gamestate->playerdown = 1; 
        else if(SDLK_w == event->key.keysym.sym) 
            gamestate->playerup = 1;
        if(SDLK_a == event->key.keysym.sym) 
            gamestate->playerleft = 1;
        else if(SDLK_d == event->key.keysym.sym) 
            gamestate->playerright = 1;
    }
        	

    if(event->type == SDL_KEYUP)
	{
        if(SDLK_s == event->key.keysym.sym) 
            gamestate->playerdown = 0;
        else if(SDLK_w == event->key.keysym.sym) 
            gamestate->playerup = 0;
        if(SDLK_a == event->key.keysym.sym) 
           	gamestate->playerleft = 0;
        else if(SDLK_d == event->key.keysym.sym) 
            gamestate->playerright = 0;
    }

    if(event->type == SDL_MOUSEMOTION)
    {
    	gamestate->mousex = event->button.x;
    	gamestate->mousey = event->button.y;
    }

    if(event->type == SDL_MOUSEBUTTONDOWN)
    {
    	if(SDL_BUTTON_LEFT == event->button.button)
			gamestate -> mousebuttonl = 1;
    		
    	if(SDL_BUTTON_RIGHT == event->button.button)
    		gamestate -> mousebuttonr = 1;
    }

   	if(event->type == SDL_MOUSEBUTTONUP)
   	{
   		if(SDL_BUTTON_LEFT == event->button.button)
    		gamestate -> mousebuttonl = 0;
    	if(SDL_BUTTON_RIGHT == event->button.button)
    		gamestate -> mousebuttonr = 0;
   	}
        	
}


void logica(Gamestate *gamestate)
{
	SDL_Rect playerRect = {	gamestate->player.x,gamestate->player.y,
							gamestate->player.w,gamestate->player.h};
	SDL_Rect parederect = {	gamestate->parede.x,gamestate->parede.y,
							gamestate->parede.w,gamestate->parede.h};
	SDL_Rect enemyRect = {	gamestate->enemy.x,gamestate->enemy.y,
							gamestate->enemy.w,gamestate->enemy.h};

	
	int Px = playerRect.x;
	int PX = playerRect.x + playerRect.w;
	int Py = playerRect.y;
	int PY = playerRect.y + playerRect.h;

	int Ex = enemyRect.x;
	int EX = enemyRect.x + enemyRect.w;
	int Ey = enemyRect.y;
	int EY = enemyRect.y + enemyRect.h;

	if(!((PX<Ex) || (EX<Px) || (PY < Ey) || (EY < Py)))
	{
		enemymove = 0;
		gamestate->player.life--;
	}
	else
	{
		enemymove = 1;
	}

	if 	(!(playerRect.y < parederect.y))
		if(gamestate->playerup == 1)
		{
			gamestate->player.y -= gamestate->player.speed;	
		}


	if (!(playerRect.y+playerRect.h > parederect.y+parederect.h))
		if(gamestate->playerdown == 1)
		{
			gamestate->player.y += gamestate->player.speed;			
		}
	
	if (!(playerRect.x < parederect.x))
		if(gamestate->playerleft == 1)
		{
			gamestate->player.x -= gamestate->player.speed;			
		}

		
	if(!(playerRect.x+playerRect.w > parederect.x+parederect.w))
		if(gamestate->playerright == 1)
		{
			gamestate->player.x += gamestate->player.speed;
		}

	if(gamestate->mousebuttonl == 1)
		{
			if(gamestate->rof == 0)
			{
				createbullet(gamestate);
				gamestate->rof = 7;
			}
		}

	updatebullet(gamestate);

	//enemy move

	if(enemymove == 1)
	{
		if (gamestate->player.x > gamestate->enemy.x)
			gamestate->enemy.x += gamestate->enemy.speed;
	    else if(gamestate->player.x < gamestate->enemy.x)
	    	gamestate->enemy.x -= gamestate->enemy.speed;
	    else;

	    if (gamestate->player.y > gamestate->enemy.y)
			gamestate->enemy.y += gamestate->enemy.speed;
	    else  if (gamestate->player.y < gamestate->enemy.y)
	    	gamestate->enemy.y -= gamestate->enemy.speed;
	    else;
	}

    int i;
    for(i = 0; i < MAXBULLETS; i++)
    {
    	SDL_Rect bulletRect = {bullet[i].x,bullet[i].y,5,5};  
	 	if(!((bulletRect.x+bulletRect.w<Ex) || (EX < bulletRect.x) || (bulletRect.y+bulletRect.h < Ey) || (EY < bulletRect.y)))
		{
	    	bullet[i].on = 0;
	    	gamestate->enemy.life -- ;

	    }
	}

	 


	if(gamestate->enemy.life <= 0)
	{
		gamestate->enemy.w = 0;
    	gamestate->enemy.h = 0;
   		gamestate->enemy.x = 0;
    	gamestate->enemy.y = 0;
    	gamestate->enemy.life = 0;
   		gamestate->enemy.score = 0;
    	gamestate->enemy.speed = 0;	
	}

	if(gamestate->rof > 0)
		gamestate->rof --;
	
   
}

void createbullet(Gamestate *gamestate)
{

	int i;
	
	for (i = 0; i < MAXBULLETS; i++)
	{
		
			if(bullet[i].on == 0)
			{	

				bullet[i].on = 1;
				bullet[i].xinit = gamestate->player.x + (gamestate->player.w/2);
				bullet[i].yinit = gamestate->player.y + (gamestate->player.h/2);
				bullet[i].x = bullet[i].xinit;
				bullet[i].y = bullet[i].yinit;
				bullet[i].destinationx = gamestate->mousex;
		    	bullet[i].destinationy = gamestate->mousey;
		    	
		    	
		    	
				return;
			}		

	}
	
}

void updatebullet(Gamestate *gamestate)
{
	SDL_Rect enemyRect = {	gamestate->enemy.x,gamestate->enemy.y,
							gamestate->enemy.w,gamestate->enemy.h};
	
	int Ex = enemyRect.x;
	int EX = enemyRect.x + enemyRect.w;
	int Ey = enemyRect.y;
	int EY = enemyRect.y + enemyRect.h;	

	int i;
	for (i = 0; i < MAXBULLETS; i++)
	{
		if(bullet[i].on == 1)
		{
			SDL_Rect bulletRect = {bullet[i].x,bullet[i].y,5,5};
			SDL_Rect parederect = {	gamestate->parede.x,gamestate->parede.y,
									gamestate->parede.w,gamestate->parede.h};


			if 	((bulletRect.y <= 100) ||
				 (bulletRect.y+bulletRect.h >= 668)||
				 (bulletRect.x <= 100)||
				 (bulletRect.x+bulletRect.w >= 924))
			{
				bullet[i].on = 0;
				
			}
			else
			{
				bullet[i].x += (bullet[i].destinationx - bullet[i].xinit) / 20;
				bullet[i].y += (bullet[i].destinationy - bullet[i].yinit) / 20;
	    	}

			
		}
		
	}
}

/*inix = 350
inity = 500
dx = 800
dy = 200

vx = 800 - 350 /10
vy = 200 -500 /10

vetor posição da bala (x,y)
vetor destino da bala (a,b)

float balapos[2] = {x,y}
float baladestiny[2] = {a,b}

 void normaliza(float*baladestiny)

normaliza(baladestiny);

float balaspeedx = baladestiny[0];

(a,b)/ sqrt((a*a) + (b*b))

void drawbullet(SDL_Renderer **renderer)
{
	
}*/